package biblioteca.test;


import biblioteca.control.BibliotecaCtrl;
import biblioteca.model.Carte;
import biblioteca.repository.repo.CartiRepo;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class AdaugaTest {

    @Test
    public void ecp1() throws Exception {
        BibliotecaCtrl ctrl = new BibliotecaCtrl(new CartiRepo());
        List<String> autori = new ArrayList<String>();
        autori.add("AuthorA");
        autori.add("AuthorB");
        List<String> keywords = new ArrayList<String>();
        keywords.add("keyword1");
        keywords.add("keyword2");
        ctrl.adaugaCarte("Book Title", autori, 2010, "Editura1", keywords);

        Carte c = ctrl.getCarti().get(ctrl.getCarti().size() - 1);
        Assert.assertEquals(c.getTitlu(), "Book Title");
        Assert.assertEquals(c.getEditura(), "Editura1");
    }

    @Test(expected = java.lang.Exception.class)
    public void ecp2() throws Exception {
        BibliotecaCtrl ctrl = new BibliotecaCtrl(new CartiRepo());
        List<String> autori = new ArrayList<String>();
        autori.add("AuthorA");
        autori.add("AuthorB");
        List<String> keywords = new ArrayList<String>();
        keywords.add("keyword1");
        keywords.add("keyword2");
        ctrl.adaugaCarte("", autori, 2010, "Editura1", keywords);
    }

    @Test(expected = java.lang.Exception.class)
    public void ecp3() throws Exception {
        BibliotecaCtrl ctrl = new BibliotecaCtrl(new CartiRepo());
        List<String> autori = new ArrayList<String>();
        autori.add("Author1");
        autori.add("Author2");
        List<String> keywords = new ArrayList<String>();
        keywords.add("keyword1");
        keywords.add("keyword2");
        ctrl.adaugaCarte("Book Title", autori, 2010, "Editura1", keywords);
    }

    @Test(expected = java.lang.Exception.class)
    public void bva1() throws Exception {
        BibliotecaCtrl ctrl = new BibliotecaCtrl(new CartiRepo());
        List<String> autori = new ArrayList<String>();
        autori.add("AuthorA");
        autori.add("AuthorB");
        List<String> keywords = new ArrayList<String>();
        keywords.add("keyword1");
        keywords.add("keyword2");
        ctrl.adaugaCarte("Book Title", autori, -1, "Editura1", keywords);
    }

    @Test(expected = java.lang.Exception.class)
    public void bva2() throws Exception {
        BibliotecaCtrl ctrl = new BibliotecaCtrl(new CartiRepo());
        List<String> autori = new ArrayList<String>();
//        autori.add("AuthorA");
//        autori.add("AuthorB");
        List<String> keywords = new ArrayList<String>();
        keywords.add("keyword1");
        keywords.add("keyword2");
        ctrl.adaugaCarte("Book Title", autori, 2010, "Editura1", keywords);
    }

    @Test
    public void bva3() throws Exception {
        BibliotecaCtrl ctrl = new BibliotecaCtrl(new CartiRepo());
        List<String> autori = new ArrayList<String>();
        autori.add("AuthorA");
        autori.add("AuthorB");
        List<String> keywords = new ArrayList<String>();
        keywords.add("keyword1");
        keywords.add("keyword2");
        ctrl.adaugaCarte("Book Title", autori, 0, "Editura1", keywords);

        Carte c = ctrl.getCarti().get(ctrl.getCarti().size() - 1);
        Assert.assertEquals(c.getTitlu(), "Book Title");
        Assert.assertEquals(c.getEditura(), "Editura1");
    }

    @Test
    public void bva4() throws Exception {
        BibliotecaCtrl ctrl = new BibliotecaCtrl(new CartiRepo());
        List<String> autori = new ArrayList<String>();
        autori.add("AuthorA");
        autori.add("AuthorB");
        List<String> keywords = new ArrayList<String>();
        keywords.add("keyword1");
        keywords.add("keyword2");
        ctrl.adaugaCarte("Book Title", autori, 1, "Editura1", keywords);

        Carte c = ctrl.getCarti().get(ctrl.getCarti().size() - 1);
        Assert.assertEquals(c.getTitlu(), "Book Title");
        Assert.assertEquals(c.getEditura(), "Editura1");
    }

    @Test
    public void bva5() throws Exception {
        BibliotecaCtrl ctrl = new BibliotecaCtrl(new CartiRepo());
        List<String> autori = new ArrayList<String>();
        autori.add("AuthorA");
        autori.add("AuthorB");
        List<String> keywords = new ArrayList<String>();
        keywords.add("keyword1");
        keywords.add("keyword2");
        ctrl.adaugaCarte("Book Title", autori, 2017, "Editura1", keywords);

        Carte c = ctrl.getCarti().get(ctrl.getCarti().size() - 1);
        Assert.assertEquals(c.getTitlu(), "Book Title");
        Assert.assertEquals(c.getEditura(), "Editura1");
    }

    @Test(expected = Exception.class)
    public void bva6() throws Exception {
        BibliotecaCtrl ctrl = new BibliotecaCtrl(new CartiRepo());
        List<String> autori = new ArrayList<String>();
        autori.add("AuthorA");
        autori.add("AuthorB");
        List<String> keywords = new ArrayList<String>();
        keywords.add("keyword1");
        keywords.add("keyword2");
        ctrl.adaugaCarte("Book Title", autori, 2019, "Editura1", keywords);
    }

    @Test
    public void bva7() throws Exception {
        BibliotecaCtrl ctrl = new BibliotecaCtrl(new CartiRepo());
        List<String> autori = new ArrayList<String>();
        autori.add("AuthorA");
        autori.add("AuthorB");
        List<String> keywords = new ArrayList<String>();
        keywords.add("keyword1");
        keywords.add("keyword2");
        ctrl.adaugaCarte("Book Title", autori, 2018, "Editura1", keywords);

        Carte c = ctrl.getCarti().get(ctrl.getCarti().size() - 1);
        Assert.assertEquals(c.getTitlu(), "Book Title");
        Assert.assertEquals(c.getEditura(), "Editura1");
    }
}
