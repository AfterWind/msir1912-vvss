package biblioteca.test;

import biblioteca.control.BibliotecaCtrl;
import biblioteca.model.Carte;
import biblioteca.repository.repo.CartiRepo;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class IntegrationBigBangTest {

    @Test
    public void unitA() throws Exception {
        BibliotecaCtrl ctrl = new BibliotecaCtrl(new CartiRepo());
        List<String> autori = new ArrayList<String>();
        autori.add("AuthorA");
        autori.add("AuthorB");
        List<String> keywords = new ArrayList<String>();
        keywords.add("keyword1");
        keywords.add("keyword2");
        ctrl.adaugaCarte("Book Title", autori, 2010, "Editura1", keywords);

        Carte c = ctrl.getCarti().get(ctrl.getCarti().size() - 1);
        Assert.assertEquals(c.getTitlu(), "Book Title");
        Assert.assertEquals(c.getEditura(), "Editura1");
    }

    @Test
    public void unitB() {
        CartiRepo repo = new CartiRepo();
        List<Carte> res = repo.cautaCarte("Ion");
        boolean found = false;
        for (Carte c : res) {
            if (c.getTitlu().equals("Amintiri din copilarie")) {
                found = true;
                break;
            }
        }
        Assert.assertTrue("Cartea nu a fost gasita desi exista in baza de date", found);
    }

    @Test
    public void unitC() {
        CartiRepo repo = new CartiRepo();
        List<Carte> res = repo.getCartiOrdonateDinAnul(2010);
        for (int i = 0 ; i < res.size() - 1; i++) {
            Assert.assertEquals("A fost gasita o carte din alt an precizat", "2010", res.get(i).getAnAparitie());
            for (int j = i + 1; j < res.size(); j++) {
                Assert.assertTrue("A fost gasita o carte in ordinea gresita", res.get(i).compareTo(res.get(j)) <= 0);
            }
        }
    }

    @Test
    public void integration() throws Exception {
        BibliotecaCtrl ctrl = new BibliotecaCtrl(new CartiRepo());
        List<String> autori = new ArrayList<String>();
        autori.add("AuthorA");
        autori.add("AuthorB");
        List<String> keywords = new ArrayList<String>();
        keywords.add("keyword1");
        keywords.add("keyword2");
        ctrl.adaugaCarte("Book Title amintiri copilarie", autori, 2010, "Editura1", keywords);

        Carte carte = ctrl.getCarti().get(ctrl.getCarti().size() - 1);
        Assert.assertEquals(carte.getTitlu(), "Book Title amintiri copilarie");
        Assert.assertEquals(carte.getEditura(), "Editura1");

        List<Carte> res = ctrl.cautaCarte("AuthorA");
        boolean found = false;
        for (Carte c : res) {
            if (c.getTitlu().equals("Book Title amintiri copilarie")) {
                found = true;
                break;
            }
        }
        Assert.assertTrue("Cartea nu a fost gasita desi exista in baza de date", found);

        res = ctrl.getCartiOrdonateDinAnul("2010");
        for (int i = 0 ; i < res.size() - 1; i++) {
            Assert.assertEquals("A fost gasita o carte din alt an precizat", "2010", res.get(i).getAnAparitie());
            for (int j = i + 1; j < res.size(); j++) {
                Assert.assertTrue("A fost gasita o carte in ordinea gresita", res.get(i).compareTo(res.get(j)) <= 0);
            }
        }
    }

}
