package biblioteca.test;

import biblioteca.model.Carte;
import biblioteca.repository.repo.CartiRepo;
import org.junit.Assert;
import org.junit.Test;

import java.util.List;

public class CautaTest {

    @Test
    public void cautaValid() {
        CartiRepo repo = new CartiRepo();
        List<Carte> res = repo.cautaCarte("Ion");
        boolean found = false;
        for (Carte c : res) {
            if (c.getTitlu().equals("Amintiri din copilarie")) {
                found = true;
                break;
            }
        }
        Assert.assertTrue("Cartea nu a fost gasita desi exista in baza de date", found);
    }

    @Test
    public void cautaInvalid() {
        CartiRepo repo = new CartiRepo();
        List<Carte> res = repo.cautaCarte("Gigi Becali");
        Assert.assertEquals("A fost gasita o carte desi nu este in baza de date niciuna", 0, res.size());
    }

    @Test
    public void cautaAnAparitie() {
        CartiRepo repo = new CartiRepo();
        List<Carte> res = repo.getCartiOrdonateDinAnul(2010);
        for (int i = 0 ; i < res.size() - 1; i++) {
            Assert.assertEquals("A fost gasita o carte din alt an precizat", "2010", res.get(i).getAnAparitie());
            for (int j = i + 1; j < res.size(); j++) {
                Assert.assertTrue("A fost gasita o carte in ordinea gresita", res.get(i).compareTo(res.get(j)) <= 0);
            }
        }
    }

}
