package biblioteca.model;


import java.util.ArrayList;
import java.util.List;

public class Carte implements Comparable<Carte> {
	
	private String titlu;
	private List<String> referenti;
	private String anAparitie;
	private String editura;
	private List<String> cuvinteCheie;
	
	public Carte(){
		titlu = "";
		referenti = new ArrayList<String>();
		anAparitie = "";
		editura = "";
		cuvinteCheie = new ArrayList<String>();
	}

	public String getTitlu() {
		return titlu;
	}

	public void setTitlu(String titlu) {
		this.titlu = titlu;
	}

	public List<String> getReferenti() {
		return referenti;
	}

	public void setReferenti(List<String> ref) {
		this.referenti = ref;
	}

	public String getAnAparitie() {
		return anAparitie;
	}

	public void setAnAparitie(String anAparitie) {
		this.anAparitie = anAparitie;
	}

	public List<String> getCuvinteCheie() {
		return cuvinteCheie;
	}

	public void setCuvinteCheie(List<String> cuvinteCheie) {
		this.cuvinteCheie = cuvinteCheie;
	}
	

	public void deleteCuvantCheie(String cuvant){
			for(int i=0;i<cuvinteCheie.size();i++){
				if(cuvinteCheie.get(i).equals(cuvant)){
					cuvinteCheie.remove(i);
					return;
				}
			}
	}
	
	public void deleteReferent(String ref){
			for(int i=0;i<referenti.size();i++){
				if(referenti.get(i).equals(ref)){
					referenti.remove(i);
					return;
				}
			}
	}
	
	public void deleteTotiReferentii(){
		referenti.clear();
	}
	
	public void adaugaCuvantCheie(String cuvant){
		cuvinteCheie.add(cuvant);
	}
	
	public void adaugaReferent(String ref){
		referenti.add(ref);
	}
	
	public boolean hasCuvantCheie(List<String> cuvinte){
		for(String c:cuvinteCheie){
			for(String cuv:cuvinte){
				if(c.equals(cuv))
					return true;
			}
		}
		return false;
	}
	 
	public boolean hasAutor(String autor){
		for(String a:referenti){
			if(a.contains(autor))
				return true;
		}
		return false;
	}
	
	@Override
	public String toString(){
		String ref = "";
		String cuvCheie = "";
		
		for(int i=0;i<referenti.size();i++){
			if(i==referenti.size()-1)
				ref+=referenti.get(i);
			else
				ref+=referenti.get(i)+",";
		}
		
		for(int i=0;i<cuvinteCheie.size();i++){
			if(i==cuvinteCheie.size()-1)
				cuvCheie+=cuvinteCheie.get(i);
			else
				cuvCheie+=cuvinteCheie.get(i)+",";
		}
		
		return titlu+";"+ref+";"+anAparitie+";"+editura+";"+cuvCheie;
	}
	
	public static Carte getCarteFromString(String carte){
		Carte c = new Carte();
		String []atr = carte.split(";");
		String []referenti = atr[1].split(",");
		String []cuvCheie = atr[4].split(",");
		
		c.titlu=atr[0];
		for(String s:referenti){
			c.adaugaReferent(s);
		}
		c.anAparitie = atr[2];
		c.editura = atr[3];
		for(String s:cuvCheie){
			c.adaugaCuvantCheie(s);
		}
		
		return c;
	}

	public String getEditura() {
		return editura;
	}

	public void setEditura(String editura) {
		this.editura = editura;
	}

	@Override
	public int compareTo(Carte b) {
		if (this.getAnAparitie().compareTo(b.getAnAparitie()) != 0) {
			return this.getAnAparitie().compareTo(b.getAnAparitie());
		}
		if (this.getTitlu().compareTo(b.getTitlu()) != 0) {
			return this.getTitlu().compareTo(b.getTitlu());
		}
		if (this.getReferenti().size() - b.getReferenti().size() != 0) {
			return this.getReferenti().size() - b.getReferenti().size();
		}
		for (int i = 0; i < this.getReferenti().size(); i++) {
			if (this.getReferenti().get(i).compareTo(b.getReferenti().get(i)) != 0) {
				return this.getReferenti().get(i).compareTo(b.getReferenti().get(i));
			}
		}
		return 0;
	}
}
