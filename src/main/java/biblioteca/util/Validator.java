package biblioteca.util;

import biblioteca.model.Carte;

import java.util.Calendar;
import java.util.Date;

public class Validator {
	
	public static boolean isStringOK(String s) throws Exception{
		boolean flag = s.matches("[a-zA-Z]+");
		if(flag == false)
			throw new Exception("String invalid");
		return flag;
	}
	
	public static void validateCarte(Carte c)throws Exception{
		if(c.getCuvinteCheie()==null){
			throw new Exception("Lista cuvinte cheie vida!");
		}
		if(c.getReferenti()==null || c.getReferenti().size() == 0){
			throw new Exception("Lista autori vida!");
		}
		if(!c.getTitlu().matches("([a-zA-Z]+ )*[a-zA-Z]+"))
			throw new Exception("Titlu invalid!");
		for(String s:c.getReferenti()){
			if(!s.matches("([a-zA-Z]+ )*[a-zA-Z]+"))
				throw new Exception("Autor " + s + " invalid!");
		}
		for(String s:c.getCuvinteCheie()){
			if(!s.matches("([a-zA-Z0-9]+ )*[a-zA-Z0-9]+"))
				throw new Exception("Cuvant cheie invalid!");
		}
		if(	!c.getAnAparitie().matches("[0-9]+") || Integer.parseInt(c.getAnAparitie()) > Calendar.getInstance().get(Calendar.YEAR))
			throw new Exception("An aparitie invalid!");
	}
	
//	public static boolean isOKString(String s){
//		String []t = s.split(" ");
//		if(t.length==2){
//			boolean ok1 = t[0].matches("[a-zA-Z]+");
//			boolean ok2 = t[1].matches("[a-zA-Z]+");
//			if(ok1==ok2 && ok1==true){
//				return true;
//			}
//			return false;
//		}
//		return s.matches("[a-zA-Z]+");
//	}
	
}
