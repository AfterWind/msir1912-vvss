package biblioteca.begin;

import biblioteca.control.BibliotecaCtrl;
import biblioteca.repository.repo.CartiRepo;
import biblioteca.repository.repoInterfaces.CartiRepoInterface;
//import biblioteca.repository.repoMock.CartiRepoMock;
import biblioteca.view.Consola;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

//functionalitati
//i.	 adaugarea unei noi carti (titlu, autori, an aparitie, editura, cuvinte cheie);
//ii.	 cautarea cartilor scrise de un anumit autor (sau parti din numele autorului);
//iii.	 afisarea cartilor din biblioteca care au aparut intr-un anumit an, ordonate alfabetic dupa titlu si autori.



public class Start {
	
	public static void main(String[] args) {
		CartiRepoInterface cr = new CartiRepo();
		BibliotecaCtrl bc = new BibliotecaCtrl(cr);
		Consola c = new Consola(bc);
		try {
//			String t = "a";
//			for (long i = 0; i < Math.log((double) Integer.MAX_VALUE / 16 + 1) / Math.log(2); i++) {
//				t += t;
//			}

//			List<String> autori = new ArrayList<String>();
//			for (long i = 0; i < Integer.MAX_VALUE / 16; i++) {
//				autori.add("AuthorA");
//			}
//			bc.adaugaCarte("T", autori, 1920, "edit1", new ArrayList<String>());

			c.executa();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
//	CartiRepoInterface cr = new CartiRepo();
//	BibliotecaCtrl bc = new BibliotecaCtrl(cr);
//	
//	Carte c = new Carte();
//	bc = new BibliotecaCtrl(cr);
//	c = new Carte();
//	
//	List<String> autori = new ArrayList<String>();
//	autori.add("Mateiu Caragiale");
//	
//	List<String> cuvinteCheie = new ArrayList<String>();
//	cuvinteCheie.add("mateiu");
//	cuvinteCheie.add("crailor");
//	
//	c.setTitlu("Intampinarea crailor");
//	c.setAutori(autori);
//	c.setAnAparitie("1948");
//	c.setEditura("Litera");
//	c.setCuvinteCheie(cuvinteCheie);
//	
//	
//	try {
//		for(Carte ca:bc.getCartiOrdonateDinAnul("1948"))
//			System.out.println(ca);
//	} catch (Exception e) {
//		// TODO Auto-generated catch block
//		e.printStackTrace();
//	}
//
//	
////	try {
////		bc.adaugaCarte(c);
////	} catch (Exception e) {
////		// TODO Auto-generated catch block
////		e.printStackTrace();
////	}
	
}
